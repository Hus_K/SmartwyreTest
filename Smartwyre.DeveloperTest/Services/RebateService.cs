﻿using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Helpers;
using Smartwyre.DeveloperTest.Interfaces;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Services;

public class RebateService : IRebateService
{
    private IRebateDataStore _rebateDataStore;
    private IProductDataStore _productDataStore;

    public RebateService(IRebateDataStore rebateDataStore, IProductDataStore productDataStore)
    {
        _rebateDataStore = rebateDataStore;
        _productDataStore = productDataStore;
    }

    public CalculateRebateResult Calculate(CalculateRebateRequest request)
    {
        Rebate rebate = _rebateDataStore.GetRebate(request.RebateIdentifier);
        Product product = _productDataStore.GetProduct(request.ProductIdentifier);

        var result = new CalculateRebateResult();

        var rebateAmount = 0m;

        if (rebate == null || product == null)
        {
            result.Success = false;
            return result;
        }

        var rebateCalculator = RebateCalculatorHelper.GetRebateCalculator(rebate.Incentive);

        if (rebateCalculator != null)
        {
            rebateAmount = rebateCalculator.CalculateRebate(rebate, product, request);

            if (rebateAmount == 0m)
            {
                result.Success = false;
                return result;
            }
            else
            {
                result.Success = true;
            }
        }
        else
        {
            result.Success = false;
        }

        if (result.Success)
        {
            _rebateDataStore.StoreCalculationResult(rebate, rebateAmount);
        }

        return result;
    }
}
