using Moq;
using Smartwyre.DeveloperTest.Interfaces;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using System;
using System.Collections.Generic;
using Xunit;

namespace Smartwyre.DeveloperTest.Tests;

public class RebateServiceTests
{
    private Mock<IProductDataStore> _productDataStore;
    private Mock<IRebateDataStore> _rebateDataStore;
    private RebateService _rebateService;
    private List<Rebate> _rebates = new List<Rebate>
    {
        new Rebate { Identifier = "1", Incentive = IncentiveType.FixedRateRebate, Amount = 3.5m, Percentage = 30 },
        new Rebate { Identifier = "2", Incentive = IncentiveType.FixedCashAmount, Amount = 3.5m, Percentage = 30 },
        new Rebate { Identifier = "3", Incentive = IncentiveType.AmountPerUom, Amount = 3.5m, Percentage = 30 }
    };
    private List<Product> _products = new List<Product>
    {
        new Product { Id = 1, Identifier = "1", Price = 13, SupportedIncentives = SupportedIncentiveType.FixedRateRebate, Uom = "Uom"}
    };

    public RebateServiceTests()
    {
        _productDataStore = new Mock<IProductDataStore>();
        _rebateDataStore = new Mock<IRebateDataStore>();
    }


    [Fact]
    public void Calculate_WithValidRequest_ShouldReturnTrue()
    {
        _rebateDataStore.Setup(r => r.GetRebate(It.IsAny<string>())).Returns(_rebates[0]);
        _rebateDataStore.Setup(r => r.StoreCalculationResult(It.IsAny<Rebate>(), It.IsAny<decimal>()));

        _productDataStore.Setup(p => p.GetProduct(It.IsAny<string>())).Returns(_products[0]);

        _rebateService = new RebateService(_rebateDataStore.Object, _productDataStore.Object);
        var request = new CalculateRebateRequest { ProductIdentifier = "1", RebateIdentifier = "1", Volume = 10 };

        var result = _rebateService.Calculate(request);

        Assert.True(result.Success);
        _rebateDataStore.Verify(r => r.StoreCalculationResult(It.IsAny<Rebate>(), It.IsAny<decimal>()));
    }

    [Fact]
    public void Calculate_WithInValidRequest_ShouldReturnFalse()
    {
        _rebateDataStore.Setup(r => r.GetRebate(It.IsAny<string>())).Returns(_rebates[1]);
        _rebateDataStore.Setup(r => r.StoreCalculationResult(It.IsAny<Rebate>(), It.IsAny<decimal>()));

        _productDataStore.Setup(p => p.GetProduct(It.IsAny<string>())).Returns(_products[0]);

        _rebateService = new RebateService(_rebateDataStore.Object, _productDataStore.Object);
        var request = new CalculateRebateRequest { ProductIdentifier = "1", RebateIdentifier = "2", Volume = 10 };

        var result = _rebateService.Calculate(request);

        Assert.False(result.Success);
        _rebateDataStore.Verify(r => r.StoreCalculationResult(It.IsAny<Rebate>(), It.IsAny<decimal>()), Times.Never);
    }
}
