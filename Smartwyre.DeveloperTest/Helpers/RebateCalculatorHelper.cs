﻿using Smartwyre.DeveloperTest.Interfaces;
using Smartwyre.DeveloperTest.Types;

namespace Smartwyre.DeveloperTest.Helpers
{
    public class RebateCalculatorHelper
    {
        public static IRebateCalculator GetRebateCalculator(IncentiveType incentiveType)
        {
            switch (incentiveType)
            {
                case IncentiveType.FixedCashAmount:
                    return new FixedCashAmountRebateCalculator();
                case IncentiveType.FixedRateRebate:
                    return new FixedRateRebateCalculator();
                case IncentiveType.AmountPerUom:
                    return new AmountPerUomRebateCalculator();
                default:
                    return null;
            }
        }
    }

    public class FixedCashAmountRebateCalculator : IRebateCalculator
    {
        public bool IsSupported(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            return product.SupportedIncentives.HasFlag(SupportedIncentiveType.FixedCashAmount);
        }

        public decimal CalculateRebate(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            if (!IsSupported(rebate, product, request) || rebate.Amount == 0)
            {
                return 0m;
            }

            return rebate.Amount;
        }
    }

    public class FixedRateRebateCalculator : IRebateCalculator
    {
        public bool IsSupported(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            return product.SupportedIncentives.HasFlag(SupportedIncentiveType.FixedRateRebate)
                && rebate.Percentage != 0
                && product.Price != 0
                && request.Volume != 0;
        }

        public decimal CalculateRebate(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            if (!IsSupported(rebate, product, request))
                return 0m;

            return product.Price * rebate.Percentage * request.Volume;
        }
    }

    public class AmountPerUomRebateCalculator : IRebateCalculator
    {
        public bool IsSupported(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            return product.SupportedIncentives.HasFlag(SupportedIncentiveType.AmountPerUom)
                && rebate.Amount != 0
                && request.Volume != 0;
        }

        public decimal CalculateRebate(Rebate rebate, Product product, CalculateRebateRequest request)
        {
            if (!IsSupported(rebate, product, request))
                return 0m;

            return rebate.Amount * request.Volume;
        }
    }
}
