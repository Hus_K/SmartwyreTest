﻿namespace Smartwyre.DeveloperTest.Interfaces
{
    public interface IConsoleApp
    {
        void Run();
    }
}
