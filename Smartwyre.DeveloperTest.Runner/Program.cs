﻿using Microsoft.Extensions.DependencyInjection;
using Smartwyre.DeveloperTest.Data;
using Smartwyre.DeveloperTest.Interfaces;
using Smartwyre.DeveloperTest.Services;
using Smartwyre.DeveloperTest.Types;
using System;

namespace Smartwyre.DeveloperTest.Runner;

public class Program : IConsoleApp
{
    private IRebateService _rebateService;

    public Program(IRebateService rebateService)
    {
        _rebateService = rebateService;
    }

    public static void Main(string[] args)
    {
        var serviceProvider = ConfigureServices();

        var app = serviceProvider.GetService<IConsoleApp>();
        app.Run();
    }

    private static IServiceProvider ConfigureServices()
    {
        var services = new ServiceCollection();

        services.AddScoped<IRebateService, RebateService>();
        services.AddScoped<IRebateDataStore, RebateDataStore>();
        services.AddScoped<IProductDataStore, ProductDataStore>();

        services.AddScoped<IConsoleApp, Program>();

        return services.BuildServiceProvider();
    }

    public void Run()
    {
        Console.WriteLine("Hello. Please enter the id of the product for rebate");
        var productId = Console.ReadLine();
        Console.WriteLine("Please enter rebate id");
        var rebateId = Console.ReadLine();
        Console.WriteLine("Please enter volume");
        var volumeString = Console.ReadLine();

        var request = new CalculateRebateRequest { ProductIdentifier = productId, RebateIdentifier = rebateId, Volume = Decimal.Parse(volumeString) };

        var result = _rebateService.Calculate(request);

        if (result.Success)
        {
            Console.WriteLine("Rebate approved");
        }
        else
        {
            Console.WriteLine("Rebate declined");
        }
    }
}
